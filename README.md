# open-sci-cosmo - open-science cosmology research

Welcome to our open-science cosmology research project.

#### Applying computational proof to galaxy curve fitting on tunable model


 * Broad: General approach to model synthesis using software + techniques from our sibling project: [open-sci-proof](https://gitlab.com/stub22/open-sci-proof/).
   * Software prototyping in progress at [gravax.fun](http://www.gravax.fun)
 * Medium: [OSC proto model use case](./osc_model_2019/osc_model_README.md) - to help motivate work in open-sci-proof.
 * Narrow: [Setting up our gravity calculation experiment](./osc_model_2019/osc_model_terms.md) - starting with some quotes, links, equations.

<br/>This image, associated with [Wikipedia page on MOND](https://en.wikipedia.org/wiki/Modified_Newtonian_dynamics), illustrates the dark energy modeling challenge.
<br/><img src="_img_quot/wikimedia/640px-M33_rotation_curve_HI.gif">
[Link to image source](https://en.wikipedia.org/wiki/File:M33_rotation_curve_HI.gif)


#### General background for cosmology inquiry

Collection of openscience resources and issues in the fields of cosmology, large scale physics, gravity, and relativity.

 * [Pop-Sci Resources](./popular_science) - Links to relatively easy reading, light on eqns.
 * [Open Questions](./open_questions) - Compiling questions of current interest;  we may also use issue tickets.
