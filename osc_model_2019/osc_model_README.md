## OSC use case sketch - for OSP 

### Introduction

The [open-sci-proof](https://gitlab.com/stub22/open-sci-proof/) project researches and prototypes 
software tools used in our cosmology modeling experiments.

This osc_model_2019 folder defines the o-s-cosmology formalization use case, to help motivate
progress by the o-s-proof efford.

### Elements of our Cosmology model to be described, formally defined, and simulated

* Foundation A:  Direct (unquantized) model of relativistic fields, gravity, energy, e.g. as captured by [A.O. Barut](https://books.google.com/books?id=gNsx7_9MJgEC) in 1964.
* Foundation B:  Meta-model of astro observation data, focusing on redshifts and observed rotation velocities of macro pheomena.
* Challenge C:  Sketch brackets on assumptions that allow conventional calculations to proceed with an estimated accuracy.
  * More plainly:  What categories of model give the same answers we get now?
* Challenge D: Pursue model-simulation challenge issued by Dr. Jan Boeyens in 2013's [Chemistry of Matter Waves](https://www.springer.com/us/book/9789400775770)

#### Describing energy and spacetime at galactic scale and above

Gregorio Baquero [2017] describes a model of galactic energy which includes a 
term for what we might fancifully call the apparent weight of delayed radiation, 
building on the Shapiro Delay concept introduced by Irwin Shapiro [1964].   

https://gregoriobaquero.wordpress.com/2017/01/26/rame-is-dm/

http://vixra.org/abs/1709.0221

Btwotwo's intuitive volumetric model is about two-thirds overlapped, 
conceptually, with Baquero's model.  We adopt his framework for describing
and testing the RAMEN hypothesis as a modeling and calculation approach.

 * [Defining Our Terms](./osc_model_terms.md) - follows Baquero's notation
   * Our working title: Galactic rotation related to "dark" energy density
