# O-S-C Model Terms: Galactic rotation velocity related to dark energy density

* Seeking progress in understanding of spacetime and energy

## Preliminaries

Background on conventional cosmology:  [Readable summary](https://astrobites.org/2016/10/11/how-big-can-galaxies-and-black-holes-get/) of Lambda Cold Dark Matter model (ΛCDM).
<br/>Beginning in the 1980s the idea of dark mass has been reformulated by Milgrom and others as MOND.
<br/>We recognize there can be some truth in each point of view, but there are deeper questions of interest.
<br/>We follow Baquero's framework [2017] of description using time dilation, 
as well as Shlomo Barak's formulation [2017] in terms of space dilation.
  * Baquero: http://vixra.org/abs/1709.0221 = 'Gravitational "Anomalies" Caused by Shapiro Delayed Traveling Energy Quanta Around Galactic and Cosmic Scale Structures'
  * Barak: https://hal.archives-ouvertes.fr/hal-01471151v4 = "A Theoretical Derivation of the Milgrom MOND Equation"

Simplified view of RAMEN hypothesis : A partial model for fitting observed galactic rotation, interpreted as externally observed mass arising from relativistic time dilation of a galactic neighborhood.  

Highlights from Baquero's abstract:<br/>
<figure>
<img src="../_img_quot/baquero_ramen_eqns/baquero_ramen_abstract_02.png"/>
<figcaption><span style="text-align: center; font-style: italic;">Image snapshot from Baquero[2017]</span></figcaption>
</figure>

* Btwotwo's response to Baquero's RAMEN:
  * Agreement on following concepts:
    * A1) Working directly with observed apparent mass, rather than modeling it as a sum of particles.
    * A2) Curve fitting of energy density function to account for non-baryonic mass.
    * A3) Time dilation as potential/partial explanation of apparent galactic mass retention or concentration.
    * A4) Open mind about actual distance, size and age of observed galaxies, and average + total energies of universe across epochs.
     
* Btwotwo's ongoing (2019-2021+) further investigation:
    * F1) Further axiomizing (encoding and formalizing) models inspired by Shapiro and Baquero.
      * http://gravax.fun = Interactive physics (Gravity+) axiom-eval, encapsulating recent software progress (based on http://axiomagic.fun)
        * "Soon": Gravax calculator page screenshot
      * Analysis of appropriate proof methods in (https://gitlab.com/stub22/open-sci-proof) = sibling gitLab project Open-Sci-Proof
    * F2) What is our family of models matching observed curves at Omega<sub>lambda</sub> ~= 6 Joules / m<sup>3</sup> ?
      * SUPPOSE we have the following 3 things:
        * T1) Hypothesis of an average energy density of 6 Joules / m<sup>3</sup>
        * T2) A set of actual observed rotation curves
        * T3) A calculation and plot that fits the curve as on page 4, using three-part legend:
          * "continuous lines indicate observed rotation curves, dashed lines indicate baryonic only rotation curves matching expected Keplerian curves, dotted lines indicates RAMEN only rotation curves" 
            * (<i>Stu B22 still has Qs about this model, pending closer reading of the two docs cited at bottom of this page</i>)
      * THEN **what is minimal logical model that requires these results** in accordance with relativistic conservation laws? 
        * Baquero's proposed "Universe Traveling Energy Density" provides a first functional we can analyze (or else we are making a mistake somewhere).
    * F3) Back out to wider view, and further axiomatize assumptions surrounding the F2 model, re: observed line width, red shift, calcs of size, age, distance, rotation velocity.

Baquero's plot, for reference:
<figure>
<img src="../_img_quot/baquero_ramen_eqns/baquero_curves_omega_six_02.png"/>
<figcaption style="text-align: center; font-style: italic;">Image snapshot from Baquero[2017]</figcaption>
</figure>

### Notes about Notation
Our equations here are written as a gumbolaya of: 
  * a) Markdown text + HTML
  * b) Programmer-friendly ascii
    * b.1) We often want to paste equations as code, and v.v.
    * b.2) Sadly, we presently avoid greek characters in our markdown text
      * Looking into [AsciiMath](http://asciimath.org/) and similar plugins.
      * Code-equations may be accompanied by a few nicely rendered eqn images
      * Would be fine to do LaTeX inside MarkDown, where is that button?
  * c)  Challenges are mainly with these notations:
    * c.1) subscripts and superscripts
    * c.2) vectors, tensors, matrices
    * c.3) derivatives and unary operators
    * c.4) sums and integrals
 
### Key terms from Baquero's RAMEN model

 * R<sub>o</sub> = Orbital Radius = written in code as <code>radOrb</code>
   * "Our integration variable is the radius (r=x) from 0 to R<sub>o</sub>" 
 * U<sub>ω<sub>(R<sub>o</sub>)</sub></sub> = "Volume of Accumulation inside Ro"  = <code>volAccum(radOrbit)</code>
 * Omega<sub>lambda</sub> = Baquero's "Universe Traveling Energy Density".
 * T<sub>mu</sub> = Baquero's "Average Time Dilation" as measured by an observer in the frame of reference orbiting the galaxy.

RAMEN equation:
<figure>
<img src="../_img_quot/baquero_ramen_eqns/baquero_ramen_abstract_05.png"/>
<figcaption style="text-align: center; font-style: italic;">Image snapshot from Baquero[2017]</figcaption>
</figure>

### Current investigation, further reading

Reviewing these papers, going deeper into observed data, statistical models, viral halo models, existing work in curve fitting.

  * https://doi.org/10.1093/mnras/stx3066 = "From light to baryonic mass: the effect of the stellar 
  mass-to-light ratio on the Baryonic Tully–Fisher relation" by A.A. Ponomareva et al, [2017]
  * https://arxiv.org/abs/1410.2256 = "Galactic rotation curves, the baryon-to-dark-halo-mass
relation and space − time scale invariance" by Xufen Wu, Pavel Kroupa [2014]
 
