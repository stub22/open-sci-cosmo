 * ["What are good, still unsolved problems in mathematical 
physics that are in vogue?"](https://mathoverflow.net/questions/275011/open-problems-in-mathematical-physics) on mathoverflow.net.
 * [Willie Wong's exposition of "Cosmic Censorship" questions](https://math.stackexchange.com/questions/50521/open-problems-in-general-relativity/50536#50536) on math.stackexchange.