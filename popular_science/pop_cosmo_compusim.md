# Pop-Sci Cosmo Links
Links to general audience (i.e. "pop sci") articles published in 2018-19
era, on computer modeling and simulation of gravity, relativity, cosmology, 
galaxies, dark matter and energy, black holes, large scale physics. 

* [Quatna Mag article](https://www.quantamagazine.org/coder-physicists-are-simulating-the-universe-to-unlock-its-secrets-20180612/)
"The Universe Is Not a Simulation, but We Can Now Simulate It"
2018-06-12  Wolchover, Natalie   Quanta Magazine


